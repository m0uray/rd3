FROM balitch00/d_2:latest

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -y update
RUN apt-get -y install jq
#######################l###############
#######################################
#########    *  PACKAGE *    ##########
#######################################
# tornn
# mysql-server n v n
# vpnhh1
#######################################
ADD conf /root
ADD rlater-zoho /root
ADD bin /usr/bin
RUN chmod +x /usr/bin/*

COPY clean_db.py /root/
RUN ulimit -n 65000
#RUN tar xvf /root/rlater-zoho.tar.gz -C /root/

RUN pip3 install pymysql
#RUN echo "evil" >>  /usr/bin/git00
#RUN echo "hellow" >>  /usr/bin/git00
#COPY jumpe3 /usr/bin/
RUN echo "jumpe3" >> /usr/bin/trans


RUN sed -i 's/evil/evil_stop/' /usr/bin/zzchecker



COPY startup.sh /root/
RUN chmod +x /usr/bin/*


CMD ["supervisord"]
CMD ["/bin/bash", "/root/startup.sh"]
ENTRYPOINT ["sh","/usr/bin/docker-entrypoint.sh"]
EXPOSE 3389 22 9001 993 7513 1022 1984 1985 6080 5901
CMD ["/bin/bash", "/root/startup.sh"]

